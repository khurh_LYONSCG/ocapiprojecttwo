var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var router = express.Router();
var rp = require('request-promise');
var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(bodyParser.json());
app.set('view engine', 'ejs');

var baseURL = 'http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v17_4/';
var clientID = '&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
var productSearch = 'product_search?q=';
var categoriesRoot3 = 'categories/root?levels=3';


app.get('/', function(req, res){
    res.render('index');
});

app.get('/categories', function(req, res){
    rp(baseURL + 'categories/root?levels=2' + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            res.send(data);
        }
    });
});

app.get('/categories/newarrivals', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var arrival = data['categories'][0];
            res.render('newArrivals', {arrival: arrival});
        }
    });
});

app.get('/categories/topsellers', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var topSellers = data['categories'][5];
            res.render('topSellers', {topSellers: topSellers});
        }
    });
});

app.get('/categories/electronics', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var electronic = data['categories'][3];
            res.render('electronics', {electronic: electronic});
        }
    });
});

app.get('/categories/mens', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var men = data['categories'][2];
            res.render('men', {men: men});
        }
    });
});

app.get('/categories/womens', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var women = data['categories'][1];
            res.render('women', {women: women});
        }
    });
});

app.get('/categories/giftcards', function(req, res){
    rp(baseURL + categoriesRoot3 + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            var giftcard = data['categories'][4];
            res.render('giftCards', {giftcard: giftcard});
        }
    });
});

// app.get('/categories/:categoryName', function(req, res){
//     var categoryName = req.params.categoryName;
//     res.send('This is the ' + categoryName + " category");
// });

app.get('/productSearch', function(req, res){
    var query = req.query.search;
    rp(baseURL + productSearch + query + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            res.render('results', {data: data});
        }
    });
});

app.get('/sony_ps3_bundle', function(req, res){
    rp(baseURL + 'products/(sony-ps3-bundle)?expand=availability,prices&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            res.render('ps3', {data: data});
        }
    });
})

app.get('/search', function(req, res){
    rp(baseURL + 'search_suggestion?q=shi&count=1&currency=EUR' + clientID, function(error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            res.send(data);
        }
    });
});

var server = app.listen(8000, function(){
    console.log('Listening on port 8000...');
});

